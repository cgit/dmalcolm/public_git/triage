#!/usr/bin/env python
from pprint import pprint
import re
import sys

import bugzilla

from rules import get_change

def main():
    bug_id = int(sys.argv[1])
    bz=bugzilla.Bugzilla(url='https://bugzilla.redhat.com/xmlrpc.cgi')
    print '---- CHANGES FOR BUG %i ----' % bug_id
    change = get_change(bz, bug_id)
    print change
    if False:
        from ui import ChangeGui
        g = ChangeGui(change)
        gtk.main()

main()



