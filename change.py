class Change(object):
    '''
    Class representing a change to be made in Bugzilla.

    Somewhat analogous to a patch to be applied to a source tree.

    Currently only supports printing, but eventually ought to support being
    applied to the bug.

    I want to capture a change as an entity so that you can always do human
    review of the change, rather than automatically pushing blindly via XML-RPC
    '''
    def __init__(self,
                 bug,
                 newsummary=None,
                 newcomponent=None,
                 comment=None,
                 duplicate_id=None,
                 status=None,
                 resolution=None):
        self.bug = bug
        self.comment = comment
        self.newsummary = newsummary
        self.newcomponent = newcomponent
        self.duplicate_id = duplicate_id
        self.status = status
        self.resolution = resolution

    def __str__(self):
        result = ''
        if self.newsummary:
            result += '---- BEGIN SUMMARY ----\n'
            result += self.newsummary
            result += '\n---- END SUMMARY ----\n'

        if self.newcomponent:
            result += '---- BEGIN COMPONENT ----\n'
            result += '"python" -> "%s"\n' % self.newcomponent # is there a way to do this via XML-RPC?
            result += '---- END COMPONENT ----\n'

        if self.comment:
            result += '---- BEGIN COMMENT ----\n'
            result += self.comment
            result += '\n---- END COMMENT ----\n'

        if self.duplicate_id:
            result += '---- MARK AS DUPLICATE OF: %i ----\n' % self.duplicate_id

        if self.status:
            result += '---- SET status: %s ----\n' % self.status

        if self.resolution:
            result += '---- SET resolution: %s ----\n' % self.resolution

        return result

class Duplicate(Change):
    def __init__(self, bug, other_bug_id):
        Change.__init__(self,
                        bug,
                        comment=(
'''Thank you for the bug report.

This particular bug has already been reported into our bug tracking system, but please feel free to report any further bugs you find.'''),
                        duplicate_id = other_bug_id
                        )

