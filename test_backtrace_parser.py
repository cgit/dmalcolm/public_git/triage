import unittest
from backtrace import Backtrace

class TestParser(unittest.TestCase):
    def test_bz677051(self):
        bt = Backtrace.from_text_file('data/rhbz-677051-attachment-478410.txt')
        
        thread, frame = bt.get_crash_site()
        self.assertEquals(thread.index, 1)
        self.assertEquals(thread.extra, '0xb78516c0 (LWP 17355)')
        self.assertEquals(len(thread.frames), 60)
        self.assertEquals(len(thread.framelist), 60)

        # Frame #13 wasn't handled by an earlier version of the parser:
        self.assert_(13 in thread.frames)

        self.assertEquals(frame.index, 0)
        self.assertEquals(frame.address, 0x4d2ade0a)
        self.assertEquals(frame.function, '_PyType_Lookup')
        self.assertEquals(frame.info,
                          "(type=0xb727bf40, name='cursor') at /usr/src/debug/Python-2.7.1/Objects/typeobject.c:2457\n"
                          "        i = <optimized out>\n"
                          "        n = <optimized out>\n"
                          "        mro = <optimized out>\n"
                          "        res = <optimized out>\n"
                          "        base = <optimized out>\n"
                          "        dict = <optimized out>\n"
                          "        h = <optimized out>")



    def test_python_backtrace(self):
        bt = Backtrace.from_text_file('data/rhbz-677051-attachment-478410.txt')

        pybt = bt.as_python_backtrace(False)

        self.assertEquals(pybt,
                          '#4 /usr/lib/python2.7/site-packages/yum/sqlitesack.py:1362 searchPrco\n'
                          '#9 /usr/lib/python2.7/site-packages/yum/sqlitesack.py:46 newFunc\n'
                          '#13 /usr/lib/python2.7/site-packages/yum/sqlitesack.py:1400 searchProvides\n'
                          '#23 /usr/lib/python2.7/site-packages/yum/packageSack.py:598 _computeAggregateListResult\n'
                          '#27 /usr/lib/python2.7/site-packages/yum/packageSack.py:426 searchProvides\n'
                          '#30 /usr/lib/python2.7/site-packages/yum/__init__.py:2990 returnPackagesByDep\n'
                          '#33 /usr/lib/python2.7/site-packages/yum/__init__.py:3313 install\n'
                          '#37 /usr/sbin/yum-complete-transaction:199 main\n'
                          '#40 /usr/sbin/yum-complete-transaction:118 __init__\n'
                          '#51 /usr/sbin/yum-complete-transaction:256 <module>\n')

    def test_infinite_recursion(self):
        bt = Backtrace.from_text_file('data/rhbz-885361-attachment-660076.txt')
        thread, frame = bt.get_crash_site()
        self.assertEqual(len(thread.frames), 2048)
        
if __name__ == '__main__':
    unittest.main()
