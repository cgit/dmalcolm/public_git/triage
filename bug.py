from pprint import pprint
import re

from backtrace import Backtrace

class Bug(object):
    def __init__(self, bz, id):
        self.bz = bz
        self.id = id
        self._bug = bz.getbug(id)
        if 1:
            pprint(self._bug.__dict__)

    def get_backtrace(self):
        # Get the backtrace associated with this ABRT bug, as a Backtrace instance
        a = self._get_backtrace_info()
        if a is None:
            raise NoBacktrace()
        return Backtrace(self.bz.openattachment(a['attach_id']).read())

    def _get_backtrace_info(self):
        for a in self._bug.attachments:
            if a['filename'] == 'backtrace':
                return a

    def get_script(self):
        '''Parse the ABRT report, extracting the script'''
        # print repr(self._bug.longdescs[0]['body'])
        for line in self._bug.longdescs[0]['body'].splitlines():
            m = re.match('cmdline: (.+)', line)
            if m:
                for arg in m.group(1).split()[1:]:
                    if arg.startswith('/'):
                        return arg
                    if arg.endswith('.py'):
                        return arg

    def get_abrt_reason(self):
        for line in self._bug.longdescs[0]['body'].splitlines():
            m = re.match('reason: (.+)', line)
            if m:
                return m.group(1)

    def get_abrt_signal(self):
        reason = self.get_abrt_reason()
        if reason is None:
            return

        m = re.match('Process was terminated by signal ([0-9]+)', reason)
        if m:
            sig = int(m.group(1))
            import signal
            for signame in dir(signal):
                if signame.startswith('SIG'):
                    if getattr(signal, signame) == sig:
                        return signame

class NoBacktrace(Exception):
    pass

