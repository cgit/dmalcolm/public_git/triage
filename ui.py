import gtk
import gtk.glade

class ChangeGui(object):
    def __init__(self, change):
        self.change = change

        self._change_wnd = gtk.glade.XML ('triage.glade', 'change_window')
        self._window = self._change_wnd.get_widget('change_window')
        self._old_summary = self._change_wnd.get_widget('old_summary')
        self._new_summary = self._change_wnd.get_widget('new_summary')
        self._old_component = self._change_wnd.get_widget('old_component')
        self._new_component = self._change_wnd.get_widget('new_component')
        self._new_comment = self._change_wnd.get_widget('new_comment')

        print self.__dict__
        for attr in self.__dict__:
            print attr
            #print getattr(self, attr).__dict__
        print self._window.__dict__
        self._window.set_title('Proposed changes for bug %i' % self.change.bug.id)
        self._old_summary.set_text(change.bug._bug.summary)
        if change.newsummary:
            self._new_summary.set_text(change.newsummary)
        self._old_component.set_text(change.bug._bug.component)
        if change.newcomponent:
            self._new_component.set_text(change.newcomponent)
        if change.comment:
            self._new_comment.get_buffer().set_text(change.comment)

