from bug import Bug, NoBacktrace
from change import Change, Duplicate

class UnsupportedComponent(Exception):
    def __init__(self, path, url):
        self.path = path
        self.url = url

def what_provides_file(path):
    '''
    Return a (subpackage, srpm) pair of names, or (None, None)
    '''

    # I'm running on 32-bit: fixup archs in path down to 32-bit version:
    path = path.replace('/lib64/', '/lib/')

    import yum
    my = yum.YumBase()
    my.setCacheDir()
    if not path.startswith('/'):
        path = '*/' + path
    for pkg, path in my.searchPackageProvides([path]).iteritems():
        print pkg.sourcerpm
        #print pkg.base_package_name
        import rpmUtils
        srpmName = rpmUtils.miscutils.splitFilename(pkg.sourcerpm)[0]
        return (pkg.name, srpmName)
    return (None, None)

def get_change(bz, bug_id):
    bug = Bug(bz, bug_id)

    signal = bug.get_abrt_signal()
    if signal:
        issue = signal
    else:
        issue = 'Crash'

    script = bug.get_script()
    # script = '/usr/bin/deluged'
    subpackage, srpmname = None, None
    if script:
        subpackage, srpmname = what_provides_file(script)


    if script and script.endswith('ies4linux-gtk.py'):
        return Change(bug,
                      newsummary='%s running %s' % (issue, 'ies4linux-gtk.py'),
                      comment=('''Thank you for your report. This bug is in the ies4linux script you are using to run Internet Explorer. Fedora does not provide or support this script. We would suggest that you report the problem to the upstream project at http://www.tatanka.com.br/ies4linux/ , but it does not seem to have been updated since February 2008, so the effort may be wasted. There is unfortunately nothing the Fedora project can do to help you with this problem.'''),
                      duplicate_id=543591,
                      )

    if subpackage is None:
        subpackage = '(unknown)'
    if srpmname is None:
        srpmname = '(unknown)'

    try:
        bt = bug.get_backtrace()
        (thread, frame) = bt.get_crash_site()
        (newsummary, bt_blurb) = characterize_bt(bug, bt, thread, script)
    except Duplicate, d:
        return d
    except NoBacktrace, e:
        comment = 'Thank you for the bug report.\n\n'
        if signal == 'SIGABRT':
            comment += 'How reproducible is this problem?  If you run the program from a terminal, is an error message printed?\n\n'

        comment += ('''Unfortunately, without a stack trace from the crash it is impossible to determine what caused the crash. Please see http://fedoraproject.org/wiki/StackTraces for more information about getting a useful stack trace with debugging symbols. Even if you cannot reproduce this crash at will, you can prepare your system now to produce a good stack trace the next time you experience the crash. 

Thank you.
''')
        return Change(bug,
                      newsummary='%s running %s' % (issue, script),
                      newcomponent = srpmname,
                      comment=comment
                      )
    except UnsupportedComponent, e:
        return Change(bug,
                      newsummary='%s in %s' % (issue, e.path),
                      comment=('''Thank you for the bug report.

Unfortunately the problem appears to be in %(path)s.

The Fedora Project only ships and maintains Free and Open Source software. Issues such as these are beyond the control of Fedora developers.  See %(url)s for more information

You may find assistance in the Fedora community support forums or mailing list, or you might consider using a commercially supported product.'''
                               % dict(path=e.path,
                                      url=e.url)
                               )
                      )

    comment = '''Thank you for reporting this bug.

How reproducible is this problem?  If you run the program from a terminal, is an error message printed?

What is the output of running the following command?
  rpm -q %(subpackage)s

%(bt_blurb)s

Reassigning component from "python" to "%(srpmname)s";  hopefully the %(srpmname)s maintainer will be able to figure this out further or reassign as necessary.
''' % dict(subpackage=subpackage,
           bt_blurb = bt_blurb,
           srpmname = srpmname)

    if newsummary in ('SIGABRT in "_XError" in /usr/share/virt-manager/virt-manager.py',
                      'SIGABRT in gdk_x_error running /usr/share/virt-manager/virt-manager.py'):
        return Duplicate(bug, 540810)

    if newsummary in ('SIGABRT in "_XError" in /usr/bin/istanbul',
                      'SIGABRT in gdk_x_error running /usr/bin/istanbul'):
        return Duplicate(bug, 543278)

    if newsummary == 'SIGABRT in "_XError" in /usr/share/ibus/ui/gtk/main.py':
        return Duplicate(bug, 546159)

    if (newsummary == 'Fatal error in "XFreeColormap" in /usr/bin/hp-systray'
        or newsummary == 'SIGSEGV in "XFreeColormap" in /usr/bin/hp-systray'):
        return Duplicate(bug, 543286)

    if newsummary == 'Crash in gtk_style_realize with "ClearlooksStyle"':
        return Duplicate(bug, 538799)

    if (newsummary == 'Fatal error in "IA__gtk_accel_groups_activate" in gajim.py'
        or newsummary == 'SIGSEGV in "IA__gtk_accel_groups_activate" in gajim.py'):
        return Duplicate(bug, 544828)

    if newsummary == 'SIGSEGV in "Py_DecRef" in /usr/bin/blueman-applet':
        return Duplicate(bug, 541002)  # should this be 536786 ???

    if newsummary == 'Assertion failure in cs_gem_write_reloc inside radeon_cs_write_reloc running /usr/bin/elisa':
        return Duplicate(bug, 546034)

    if newsummary =='SIGSEGV in "setup_primary_label_font" in /usr/bin/scribes':
        return Duplicate(bug, 547971)

    if newsummary == 'Incorrect thread usage in Connection_end_allow_threads running /usr/share/system-config-printer/system-config-printer.py':
        return Duplicate(bug, 542866)

    if newsummary == 'SIGSEGV in "SMBC_parse_path" in /usr/share/system-config-printer/system-config-printer.py':
        return Duplicate(bug, 552658)

    if newsummary == 'SIGABRT in "__fortify_fail" in /usr/bin/gwibber-daemon':
        return Duplicate(bug, 539809)

    if (newsummary == 'SIGABRT in "IA__g_logv" in /usr/lib/glipper/glipper'
        or newsummary == 'SIGABRT in "IA__g_logv" in /usr/lib64/glipper/glipper'):
        return Duplicate(bug, 544744)

    if newsummary == 'SIGSEGV in "IA__gdk_cairo_set_source_pixbuf" in /usr/bin/gnochm':
        return Duplicate(bug, 552417)

    if script and script.endswith('SABnzbd.py'):
        return Duplicate(bug, 552765)

    if script == 'ubuntu-tweak.py':
        # Getting various SIGABRT and SIGSEGV from people running "ubuntu-tweak.py"
        # See https://bugzilla.redhat.com/buglist.cgi?quicksearch=ubuntu-tweak
        return Change(bug,
                      newsummary = newsummary,
                      status='CLOSED', resolution='UPSTREAM',
                      comment = ('''Thank you for the bug report.  This problem appears to be related to the
ubuntu-tweak.py script.  This script is not yet packaged within Fedora.  The
best way to make sure your problem will get looked on is to report it to the
authors of the program. Most upstream authors use a bug tracking system like
Bugzilla, and more people who know the code will be looking at the bug report
there.
The upstream bug tracking system to use is:
  https://bugs.launchpad.net/ubuntu-tweak/
You are requested to add the bugzilla link here for tracking purposes. Please
make sure the bug isn't already in the upstream bug tracker before filing it.

If you would like to see this software packaged for Fedora, you might want to
add it to the wishlist here:
https://fedoraproject.org/wiki/Package_maintainers_wishlist if it isn't there
already.''')
                      )

    if (script == '/usr/share/system-config-printer/system-config-printer.py'
        and signal == 'SIGSEGV'
        and frame.info.endswith('libsmbclient.so.0')):
        # bug 546891 - SIGSEGV inside /usr/lib/libsmbclient.so.0 in /usr/share/system-config-printer/system-config-printer.py
        return Duplicate(bug, 546891)

    # Other possible dups:
    # bug 548849 - SIGSEGV in yumex due to infinite recursion in "temporary_disable_extension_events" in gdk_window_ensure_native called by /usr/lib/gtk-2.0/2.10.0/immodules/im-xim.so

    ch = Change(bug,
                newsummary = newsummary,
                newcomponent = srpmname,
                comment = comment
                )

    print '---- BEGIN THREAD ----'
    for id in sorted(thread.frames.keys()):
        f = thread.frames[id]
        if f.address:
            addr = hex(f.address)
        else:
            addr = None
        print '#%i %s %s %s' % (id, addr, f.function, f.info[:100])
    print '---- END THREAD ----'

    return ch

def characterize_bt(bug, bt, thread, script):
    '''Get (newsubject, commentblurb)'''
    bt_blurb = 'Looking at the backtrace, it looks like '
    function = None

    signal = bug.get_abrt_signal()
    if signal:
        issue = signal
    else:
        issue = 'Fatal error'

    for (i, frame) in enumerate(thread.framelist):
        # Get function name for deepest point in stack that has one:
        if function is None or function in ['??', 'vtable', '__kernel_vsyscall', 'raise', 'abort', 'g_log', 'g_logv']:
            function = frame.function

        if frame.function == 'gtk_style_realize':
            if 'ClearlooksStyle' in frame.info:
                return ('Crash in gtk_style_realize with "ClearlooksStyle"', None)

        if frame.function == 'abort':
            # print 'got abort!'
            for j in range(i, len(thread.framelist)):
                if thread.framelist[j].function == 'gdk_x_error':
                    return ('%s in gdk_x_error running %s' % (issue, script),
                            (bt_blurb + 'a fatal error happened in frame %(f_idx)s of %(thread)s inside gdk_x_error.'
                             % dict(thread = describe_thread(bt, thread),
                                    f_idx = j)
                             )
                            )

                if thread.framelist[j].function == '__assert_fail':
                    return (('Assertion failure in %s inside %s running %s'
                             % (thread.framelist[j+1].function,
                                thread.framelist[j+2].function,
                                script)
                             ),
                            (bt_blurb + 'an assertion failed inside frame %(f_idx)s of %(thread)s inside %(function)s.'
                             % dict(thread = describe_thread(bt, thread),
                                    f_idx = j,
                                    function = thread.framelist[j+1].function)
                             )
                            )

        if frame.function in ['_XError', '__fortify_fail']:
            function = frame.function

        if frame.function in ['IA__g_assertion_message_expr']:
            function = thread.framelist[i+1].function

        if frame.function == 'Py_FatalError':
            if thread.framelist[i+1].function == 'PyEval_RestoreThread':
                    return (('Incorrect thread usage in %s running %s'
                             % (thread.framelist[i+2].function,
                                script)
                             ),
                            (bt_blurb + "an incorrect usage of Python's internal thread API was detected in %(function)s in frame #%(f_idx)s of %(thread)s"
                             % dict(function = thread.framelist[i+2].function,
                                    thread = describe_thread(bt, thread),
                                    f_idx = i+2)
                             )
                            )

        if frame.info == '() from /usr/lib/flash-plugin/libflashplayer.so':
            raise UnsupportedComponent('/usr/lib/flash-plugin/libflashplayer.so',
                                       'https://fedoraproject.org/wiki/ForbiddenItems#Adobe_Flash_Player')

    bt_blurb += 'the problem occurred in %s' % describe_thread(bt, thread)
    if function:
        bt_blurb += ' in %s' % function

    if len(thread.framelist) == 1:
        bt_blurb += '.  However the short length of the backtrace suggests that there was a problem generating the backtrace, so that may not be the true location of the error.'

    if function:
        summary = '%s in "%s" in %s' % (issue, function, script)
    else:
        summary = '%s in %s' % (issue, script)

    return (summary, bt_blurb)

def describe_thread(bt, thread):
    if len(bt._threads) == 1:
        return "the program's single thread"
    else:
        return "thread #%i" % thread.index
